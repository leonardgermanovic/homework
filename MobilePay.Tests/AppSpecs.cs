using System;
using Xunit;
using Danske.MobilePay;
using System.Text;
using System.IO;
using NSubstitute;
using System.Collections.Generic;

namespace Danske.MobilePay.Tests
{
    public class ProgramSpecs
    {
        [Fact]
        public void AppRun_Given4Transactions_CalculatesOnePercentFree()
        {
            // ARRANGE
            const string data = @"
2018-09-02 CIRCLE_K 120
2018-09-04 TELIA 200

2018-10-22  CIRCLE_K 300
2018-10-29 CIRCLE_K 150";
            byte[] byteArray = Encoding.ASCII.GetBytes(data);
            using var stream = new MemoryStream(byteArray);
            var output = Substitute.For<IOutput>();
            var app = Create(output, null);

            // ACT
            app.Run(stream);

            // ASSSERT
            output.Received().WriteLine("2018-09-02 CIRCLE_K 29.96");
            output.Received().WriteLine("2018-09-04 TELIA    30.80");
            output.Received().WriteLine("2018-10-22 CIRCLE_K 31.40");
            output.Received().WriteLine("2018-10-29 CIRCLE_K 1.20");
        }

        [Fact]
        public void AppRun_Given4TeliaTransactions_CalculateTenPercentFreeWithFixedInvoice()
        {
            // ARRANGE
            var t1 = new TransactionDto { Date = new DateTimeOffset(2018, 9, 2, 0, 0, 0, TimeSpan.Zero), MerchantName = "TELIA", Amount = 120 };
            var t2 = new TransactionDto { Date = new DateTimeOffset(2018, 9, 4, 0, 0, 0, TimeSpan.Zero), MerchantName = "TELIA", Amount = 200 };
            var t3 = new TransactionDto { Date = new DateTimeOffset(2018, 10, 22, 0, 0, 0, TimeSpan.Zero), MerchantName = "TELIA", Amount = 300 };
            var t4 = new TransactionDto { Date = new DateTimeOffset(2018, 10, 29, 0, 0, 0, TimeSpan.Zero), MerchantName = "TELIA", Amount = 150 };

            var output = Substitute.For<IOutput>();
            var parser = Substitute.For<ITransactionParser>();
            parser.ParseStream(Arg.Any<Stream>()).Returns(new List<TransactionDto> { t1, t2, t3, t4 });
            var app = Create(output, parser);

            // ACT
            app.Run(null);

            // ASSSERT
            output.Received().WriteLine("2018-09-02 TELIA    30.08");
            output.Received().WriteLine("2018-09-04 TELIA    1.80");
            output.Received().WriteLine("2018-10-22 TELIA    31.70");
            output.Received().WriteLine("2018-10-29 TELIA    1.35");
        }

        [Fact]
        public void AppRun_GivenCirkleK4Transactions_CalculateTwentyPercentFreeWithFixedInvoice()
        {
            // ARRANGE
            var t1 = new TransactionDto { Date = new DateTimeOffset(2018, 9, 2, 0, 0, 0, TimeSpan.Zero), MerchantName = "CIRCLE_K", Amount = 120 };
            var t2 = new TransactionDto { Date = new DateTimeOffset(2018, 9, 4, 0, 0, 0, TimeSpan.Zero), MerchantName = "CIRCLE_K", Amount = 200 };
            var t3 = new TransactionDto { Date = new DateTimeOffset(2018, 10, 22, 0, 0, 0, TimeSpan.Zero), MerchantName = "CIRCLE_K", Amount = 300 };
            var t4 = new TransactionDto { Date = new DateTimeOffset(2018, 10, 29, 0, 0, 0, TimeSpan.Zero), MerchantName = "CIRCLE_K", Amount = 150 };

            var output = Substitute.For<IOutput>();
            var parser = Substitute.For<ITransactionParser>();
            parser.ParseStream(Arg.Any<Stream>()).Returns(new List<TransactionDto> { t1, t2, t3, t4 });
            var app = Create(output, parser);

            // ACT
            app.Run(null);

            // ASSSERT
            output.Received().WriteLine("2018-09-02 CIRCLE_K 29.96");
            output.Received().WriteLine("2018-09-04 CIRCLE_K 1.60");
            output.Received().WriteLine("2018-10-22 CIRCLE_K 31.40");
            output.Received().WriteLine("2018-10-29 CIRCLE_K 1.20");
        }

        [Fact]
        public void AppRun_GivenFourTransactions_CalculateFeeWithInvoiceFixedFee()
        {
            // ARRANGE
            var t1 = new TransactionDto { Date = new DateTimeOffset(2018, 9, 2, 0, 0, 0, TimeSpan.Zero), MerchantName = "7-ELEVEN", Amount = 120 };
            var t2 = new TransactionDto { Date = new DateTimeOffset(2018, 9, 4, 0, 0, 0, TimeSpan.Zero), MerchantName = "NETTO", Amount = 200 };
            var t3 = new TransactionDto { Date = new DateTimeOffset(2018, 10, 22, 0, 0, 0, TimeSpan.Zero), MerchantName = "7-ELEVEN", Amount = 300 };
            var t4 = new TransactionDto { Date = new DateTimeOffset(2018, 10, 29, 0, 0, 0, TimeSpan.Zero), MerchantName = "7-ELEVEN", Amount = 150 };

            var output = Substitute.For<IOutput>();
            var parser = Substitute.For<ITransactionParser>();
            parser.ParseStream(Arg.Any<Stream>()).Returns(new List<TransactionDto> { t1, t2, t3, t4 });
            var app = Create(output, parser);

            // ACT
            app.Run(null);

            // ASSSERT
            output.Received().WriteLine("2018-09-02 7-ELEVEN 30.20");
            output.Received().WriteLine("2018-09-04 NETTO    31.00");
            output.Received().WriteLine("2018-10-22 7-ELEVEN 32.00");
            output.Received().WriteLine("2018-10-29 7-ELEVEN 1.50");
        }

        private App Create(IOutput output, ITransactionParser parser)
        {
            var settings = new Settings();
            if (parser == null)
            {
                parser = new TransactionParser(new CsvParserFactory(), settings);
            }
            return new App(output, new TransactionFeeCalculator(settings, new MerchantMonthlyRevenue()), new TransactionFeeFormatter(settings), parser);
        }
    }
}
