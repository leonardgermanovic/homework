﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;

namespace Danske.MobilePay
{
    public interface ITransactionParser : IDisposable
    {
        IEnumerable<TransactionDto> ParseStream(Stream stream);
    }

    public class TransactionParser : ITransactionParser, IDisposable
    {
        private readonly ICsvParserFactory _csvParserFactory;
        private readonly Settings _settings;

        private bool _disposed = false;
        private TextReader _reader;

        public TransactionParser(ICsvParserFactory csvParserFactory, Settings settings)
        {
            _csvParserFactory = csvParserFactory;
            _settings = settings;
        }

        public IEnumerable<TransactionDto> ParseStream(Stream stream)
        {
            _reader = new StreamReader(stream, _settings.DefaultFileEncoding);

            var csvParser = _csvParserFactory.Create(_reader, _settings.Delimiter);
            var data = csvParser.Parse();
            return ParseTransactions(data);
        }

        private IEnumerable<TransactionDto> ParseTransactions(IEnumerable<IList<string>> transactions)
        {
            foreach (var record in transactions)
            {
                var transaction = new TransactionDto
                {
                    Date = DateTime.ParseExact(record[0], _settings.DateFormat, _settings.CultureInfo),
                    MerchantName = record[1],
                    Amount = decimal.Parse(record[2], _settings.CultureInfo)
                };
                yield return transaction;
            }
        }

        public void Dispose()
        {
            if (!this._disposed)
            {
                _reader?.Dispose();
                _disposed = true;
            }

            GC.SuppressFinalize(this);
        }
    }
}
