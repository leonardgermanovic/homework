﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Danske.MobilePay
{
    public interface IOutput
    {
        public void WriteLine(string value);
    }

    public class ConsoleOutput : IOutput
    {
        public void WriteLine(string value)
        {
            Console.WriteLine(value);
        }
    }
}
