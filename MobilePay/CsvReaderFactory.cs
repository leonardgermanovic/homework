﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Danske.MobilePay
{
    public interface ICsvParserFactory
    {
        CsvParser Create(TextReader reader, char delimiter);
    }

    public class CsvParserFactory : ICsvParserFactory
    {
        public CsvParser Create(TextReader reader, char delimiter)
        {
            return new CsvParser(reader, delimiter);
        }
    }
}
