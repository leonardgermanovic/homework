﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Danske.MobilePay
{
    public class TransactionFeeDto
    {
        public DateTimeOffset Date { get; set; }

        public string MerchantName { get; set; }

        public decimal Fee { get; set; }
    }
}
