﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Danske.MobilePay
{
    public class Settings
    {
        public string DateFormat = "yyyy-MM-dd";

        public CultureInfo CultureInfo = CultureInfo.InvariantCulture;

        public string DecimalFormat = "0.00";

        public char Delimiter = ' ';

        public string DefaultFileName = "transactions.txt";

        public Encoding DefaultFileEncoding = Encoding.ASCII;

        public int MerchantFixedLength = 8;

        public decimal InvoiceFixedFee = 29;
    }
}
