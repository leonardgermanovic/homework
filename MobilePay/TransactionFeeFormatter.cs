﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Danske.MobilePay
{
    public class TransactionFeeFormatter
    {
        private readonly Settings _settings;

        public TransactionFeeFormatter(Settings settings)
        {
            _settings = settings;
        }

        public string Format(TransactionFeeDto fee)
        {
            return string.Format("{0}{1}{2}{3}{4}",
                fee.Date.ToString(_settings.DateFormat, _settings.CultureInfo),
                _settings.Delimiter,
                fee.MerchantName.PadRight(_settings.MerchantFixedLength, _settings.Delimiter),
                _settings.Delimiter,
                fee.Fee.ToString(_settings.DecimalFormat, _settings.CultureInfo));
        }
    }
}
