﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Danske.MobilePay
{
    public class MerchantMonthlyRevenue
    {
        private readonly Dictionary<string, Dictionary<int, decimal>> _revenue = new Dictionary<string, Dictionary<int, decimal>>();

        public decimal AddRevenue(string merchantName, DateTimeOffset transactionDate, decimal transactionAmount)
        {
            Dictionary<int, decimal> merchantRevenue;

            if (!_revenue.TryGetValue(merchantName, out merchantRevenue))
            {
                merchantRevenue = new Dictionary<int, decimal>
                {
                    { GetMonthPeriod(transactionDate), transactionAmount }
                };
                _revenue.Add(merchantName, merchantRevenue);
                return transactionAmount;
            }
            else
            {
                var monthPeriod = GetMonthPeriod(transactionDate);
                decimal merchantMonthRevenue;
                if (!merchantRevenue.TryGetValue(monthPeriod, out merchantMonthRevenue))
                {
                    merchantRevenue.Add(GetMonthPeriod(transactionDate), transactionAmount);
                    return transactionAmount;
                }
                else
                {
                    merchantMonthRevenue += transactionAmount;
                    merchantRevenue[monthPeriod] = merchantMonthRevenue;
                    return merchantMonthRevenue;
                }
            }
        }

        private int GetMonthPeriod(DateTimeOffset transactionDate)
        {
            return (transactionDate.Year * 100) + transactionDate.Month;
        }
    }
}
