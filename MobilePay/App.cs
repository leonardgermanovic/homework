﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Danske.MobilePay
{
    public class App
    {
        private readonly IOutput _output;
        private readonly TransactionFeeCalculator _calculator;
        private readonly TransactionFeeFormatter _formatter;
        private readonly ITransactionParser _parser;

        public App(IOutput output, TransactionFeeCalculator calculator, TransactionFeeFormatter formatter, ITransactionParser parser)
        {
            _output = output;
            _calculator = calculator;
            _formatter = formatter;
            _parser = parser;
        }

        public static void Main(string[] args)
        {
            var settings = new Settings();
            var fileName = args.Length > 0 ? args[0] : settings.DefaultFileName;

            using (var fileStream = CreateStreamFromFile(fileName))
            {
                var app = Create();
                app.Run(fileStream);
            }

            Console.ReadLine();
        }

        public static App Create()
        {
            var settings = new Settings();
            return new App(new ConsoleOutput(), new TransactionFeeCalculator(settings, new MerchantMonthlyRevenue()), new TransactionFeeFormatter(settings), new TransactionParser(new CsvParserFactory(), settings));
        }

        public static Stream CreateStreamFromFile(string fileName)
        {
            return File.Open(fileName, FileMode.Open, FileAccess.Read, FileShare.None);
        }

        public void Run(Stream fileStream)
        {
            using (_parser)
            {
                var transactions = _parser.ParseStream(fileStream);
                var fees = _calculator.CalculateFee(transactions);

                foreach (var fee in fees)
                {
                    var feeFormatted = _formatter.Format(fee);
                    _output.WriteLine(feeFormatted);
                }
            }
        }
    }
}
