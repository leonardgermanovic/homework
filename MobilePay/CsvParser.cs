﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Danske.MobilePay
{
    public class CsvParser
    {
        private readonly char _delimiter;
        private readonly TextReader _reader;

        public CsvParser(TextReader reader, char delimiter)
        {
            _reader = reader;
            _delimiter = delimiter;
        }

        public IEnumerable<IList<string>> Parse()
        {
            var record = new List<string>();
            var sb = new StringBuilder();

            while (_reader.Peek() != -1)
            {
                var readChar = (char)_reader.Read();

                if (readChar == '\n' || (readChar == '\r' && (char)_reader.Peek() == '\n'))
                {
                    if (readChar == '\r')
                    {
                        _reader.Read();
                    }

                    if (record.Count > 0 || sb.Length > 0)
                    {
                        record.Add(sb.ToString());
                        sb.Clear();
                    }

                    if (record.Count > 0)
                    {
                        yield return record;
                    }

                    record = new List<string>(record.Count);
                }
                else if (sb.Length == 0)
                {
                    if (readChar == _delimiter && sb.Length > 0)
                    {
                        record.Add(sb.ToString());
                        sb.Clear();
                    }
                    else if (char.IsWhiteSpace(readChar))
                    {
                        // Ignore leading whitespace
                    }
                    else
                    {
                        sb.Append(readChar);
                    }
                }
                else if (readChar == _delimiter)
                {
                    record.Add(sb.ToString());
                    sb.Clear();
                }
                else
                {
                    sb.Append(readChar);
                }
            }

            if (record.Count > 0 || sb.Length > 0)
            {
                record.Add(sb.ToString());
            }

            if (record.Count > 0)
            {
                yield return record;
            }
        }
    }
}
