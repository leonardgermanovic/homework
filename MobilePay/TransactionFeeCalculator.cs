﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Danske.MobilePay
{
    public class TransactionFeeCalculator
    {
        private readonly Settings _settings;
        private readonly MerchantMonthlyRevenue _revenue;

        public TransactionFeeCalculator(Settings settings, MerchantMonthlyRevenue revenue)
        {
            _settings = settings;
            _revenue = revenue;
        }

        public IEnumerable<TransactionFeeDto> CalculateFee(IEnumerable<TransactionDto> transactions)
        {
            foreach (var transaction in transactions)
            {
                var fee = Math.Round(transaction.Amount / 100, 2);

                if (transaction.MerchantName.Equals("TELIA"))
                {
                    var feeDiscount = Math.Round(fee * 0.1m, 2);
                    fee -= feeDiscount;
                }
                else if (transaction.MerchantName.Equals("CIRCLE_K"))
                {
                    var feeDiscount = Math.Round(fee * 0.2m, 2);
                    fee -= feeDiscount;
                }

                var merchantMonthRevenue = _revenue.AddRevenue(transaction.MerchantName, transaction.Date, transaction.Amount);
                if (merchantMonthRevenue == transaction.Amount && fee > 0)
                {
                    fee += _settings.InvoiceFixedFee;
                }

                var transactionFee = new TransactionFeeDto
                {
                    Date = transaction.Date,
                    MerchantName = transaction.MerchantName,
                    Fee = fee
                };
                yield return transactionFee;
            }
        }
    }
}
